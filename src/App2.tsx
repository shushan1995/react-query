import React, { useState } from 'react';
import { useMutation } from 'react-query';
import axios from 'axios';

interface typeForMutate {
    id: number;
    title: string;
    description: string;
}

export default function Post() {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [message, setMessage] = useState('');

    const { isLoading, isError, error, mutate } = useMutation(createPost, { retry: 3 })

    async function createPost() {

        // const rawResponse = await fetch('https://jsonplaceholder.typicode.com/posts', {
        //     method: 'POST',
        //     // headers: {
        //     //     'Accept': 'application/json',
        //     //     'Content-Type': 'application/json'
        //     // },
        //     // body: JSON.stringify({})
        // });
        // const response = await rawResponse.json();
        // setMessage(response.data)
        const response = await axios.post('https://jsonplaceholder.typicode.com/posts')
        setMessage(response.data)
    }
    return (
        <>
            <div>
                <h1>Post</h1>
                <label>Title:</label>
                {/* <input type="text" value={title} onChange={(e: any) => setTitle(e.target.value)} />
                <label>Description</label>
                <input type="text" value={description} onChange={(e: any) => setDescription(e.target.value)} />

                <button onClick={() => { mutate({ id: Date.now(), title, description }) }}>Create</button>
                <p>Create a new Post ID:{message && message.id}</p>
                <div style={{ color: 'gray', background: '#234' }}>
                    {isLoading ? "Saving..." : ""
                    }
                    {isError ? error.message : ""
                    }
                </div> */}
            </div>
        </>
    )
}