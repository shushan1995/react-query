import React from 'react';
// import logo from './logo.svg';
import { useQuery } from 'react-query'
import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.tsx</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

async function dataJson() {
  // var data: any
  const a = await fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(a => {
      // data = a
      return a
    });
  // .then(res => {
  //   console.log("here>>", JSON.stringify(res))
  //   return res.json
  // })
  console.log("data here>>", JSON.stringify(a))
  return a;
}
function App() {
  const { data, error, isError, isLoading } = useQuery('posts', dataJson)
  if (isLoading) {
    return <div>Loading...</div>
  }
  if (isError) {
    return <div>Error!</div>
  }

  return (
    <div>
      <h1>Posts</h1>
      {
        data.map((post: any, index: number) => {
          return <li key={index}>{post.title}</li>
        })
      }
    </div>
  )
}

export default App;
